window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        document.querySelector("container").innerHTML += `
        <div class="alert alert-primary" role="alert">
          Unable to load conferences!
        </div>
    `;
      } else {
        const data = await response.json();
        let count = 3;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts).toLocaleDateString();
            const endDate = new Date(details.conference.ends).toLocaleDateString();
            const location = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            const column = document.querySelector(`.row > :nth-child(${count % 3 + 1})`);
            column.innerHTML += html;
          }
          count++;
        }

      }
    } catch (e) {
        document.querySelector("container").innerHTML += `
      <div class="alert alert-primary" role="alert">
        Unable to load conferences!
      </div>
    `;
    }

  });


  function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card m-2 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${startDate} - ${endDate}
        </div>
      </div>
    `;
  }
