window.addEventListener('DOMContentLoaded', async () => {
    const API_URL = 'http://localhost:8000/api/locations/'

    try {
        const response = await fetch(API_URL);

        if (!response.ok) {
            console.error("THIS ERROR")
        } else {
            const data = await response.json();
            const selectTag = document.getElementById('location');

            for (let location of data.locations) {
                let option = document.createElement('option');
                option.value = location.id;
                option.innerText = location.name;
                selectTag.appendChild(option);
            }

            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                console.log(json);
                const CONFERENCE_URL = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                  method: "post",
                  body: json,
                  headers: {
                    'Content-Type': 'application/json',
                  },
                };
                const response = await fetch(CONFERENCE_URL, fetchConfig);
                if (response.ok) {
                  formTag.reset();
                  const newConference = await response.json();
                  console.log(newConference);
                }
            });
        }
    } catch (e) {
        console.error(e)
    }
});
