window.addEventListener('DOMContentLoaded', async () => {

    const API_URL = 'http://localhost:8000/api/states/'

    try {
        const response = await fetch(API_URL);

        if (!response.ok) {
            console.error("ERROR")
        } else {
            const data = await response.json();
            const selectTag = document.getElementById('state');
            console.log(data.states)

            for (let state of data.states) {
                let option = document.createElement('option');
                option.value = Object.values(state);
                option.innerText = Object.keys(state);
                selectTag.appendChild(option);
            }

            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const LOCATION_URL = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                  method: "post",
                  body: json,
                  headers: {
                    'Content-Type': 'application/json',
                  },
                };
                const response = await fetch(LOCATION_URL, fetchConfig);
                if (response.ok) {
                  formTag.reset();
                  const newLocation = await response.json();
                  console.log(newLocation);
                }
            });
        }
    } catch (e) {
        console.error(e)
    }
});
